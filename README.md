# ES6 Number Polyfill #

A JavaScript polyfill for ES6 Number functions / "constants".

Mostly a collection of polyfilled functions retrieved from [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Number).

### Set Up ###

1. Download the latest release (as of 9 Jul 2015, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="es6-number-polyfill-1.0.0.min.js"></script>`

### API ###

Refer to [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Number).

### FAQ ###

1. Does this polyfill have any bugs/limitations?

> Not that we know of. If you find any bugs/limitations, please let us know.
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.