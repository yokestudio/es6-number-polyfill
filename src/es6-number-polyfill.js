/**
 * Polyfills for ES6 Number functions / constants
 * - isNaN()
 * - isFinite()
 * - isInteger()
 * - isSafeInteger()
 * - parseFloat(), parseInt()
 * - MIN_SAFE_INTEGER, MAX_SAFE_INTEGER
 * - EPSILON
 */
Number['isNaN'] = Number['isNaN'] || function(v) {
	return typeof v === 'number' && v !== v;
};
Number['isFinite'] = Number['isFinite'] || function(v) {
	return typeof v === 'number' && isFinite(v);
};
Number['isInteger'] = Number['isInteger'] || function(v) {
	return typeof value === 'number' && 
	       isFinite(v) && 
	       Math.floor(v) === v;
};
Number['isSafeInteger'] = Number['isSafeInteger'] || function (v) {
	return typeof v === 'number' && v === v && // handle NaNs
	       v !== Infinity && v !== -Infinity && // handle infinite numbers
	       Math.floor(v) === v && // handle floats
	       v >=-9007199254740991 && v <= 9007199254740991; // ensure within range
};
Number['parseFloat'] = parseFloat;
Number['parseInt'] = parseInt;
Number['MIN_SAFE_INTEGER'] = -9007199254740991;
Number['MAX_SAFE_INTEGER'] = 9007199254740991;
Number['EPSILON'] = 2.220446049250313e-16;